<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {
    public function test_removePoneyFromField() {
      // Setup
      $Poneys = new Poneys();

      // Action
     
      $Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $Poneys->getCount());
   
    }
	/**
	* @dataProvider fournisseur
	*/
public function test_removeLimitPoneyFromField($a,$expected) {
      // Setup
      $Poneys = new Poneys();

      // Action
     
      $Poneys->removePoneyFromField($a);
      
      $this->assertEquals($expected, $Poneys->getCount()); 
    }

    

	/**
	* @expectedException Exception
	*/
    public function test_removeAllPoneyFromFieldException() {
      // Setup
      $Poneys = new Poneys();

      // Action
     
      $Poneys->removePoneyFromField(15);
    }

    public function fournisseur()
    {
        return array(
          array(4,4),
          array(8,0),
          array(2,6),
          array(1,7)
        );
    }
    public function testBouchon()
    {
        // Créer un bouchon pour la classe UneClasse.
        $bouchon = $this->getMock('Poneys');

        // Configurer le bouchon.
        $bouchon->expects($this->any())
             ->method('getNames')
             ->will($this->returnValue('\n'));

        }
  public function setUp(){
  $Poneys = new Poneys();
  

}
  }
 ?>
